const express = require('express');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const uuidv1 = require('uuid/v1');
const fs = require('fs');
const app = express();
const promBundle = require("express-prom-bundle");
const promClient = require("prom-client");

//Adding the prometheus watch points
const bundle = promBundle({
    buckets: [0.1,0.4,0.7,1,5,10],
    promClient: {
        collectDefaultMetrics: {
            temout: 1000
        }
    }
});

app.use(bundle);

const numBlabs = new promClinet.Counter({name: 'total_blabs_created_count', help: 'Counts the number of blabs that have been created'})

//'mongodb://mongo:27017'

var mongoUrl = fs.readFileSync('/run/secrets/mongoUrl', 'utf8');
console.log(mongoUrl);
let mongoDb = null;

app.use(bodyParser.json());

app.get('/blabs', (req,res) => {
    var time = 0;
    if(req.query.createdSince !== undefined) {
        time = req.query.createdSince;
    }
    var query = { postTime: { $gt : eval(time)}};
    mongoDb.collection('blabs')
        .find(query).toArray()
        .then(function(items) {
            res.statusCode = 200;
            res.send(items);
        });
});

app.post('/blabs', (req,res) => {
    const newItem = {
        id : uuidv1(),
        postTime : (new Date() / 1000),
        author : req.body.author,
        message : req.body.message
    };

    mongoDb.collection('blabs')
        .insertOne(newItem)
        .then(function(response) {
            console.log(newItem.postTime);
            res.statusCode = 201;
            res.json({
                id : newItem.id,
                postTime : newItem.postTime,
                author : newItem.author,
                messege : newItem.message
            });
        });
    numBlabs.inc(1);
});

app.delete('/blabs/{id}', (req,res) => {
    let answer = mongoDb.colection.remove();
    if(answer.nRemoved > 0) {
        code = 200;
    }
    else {
        res.statusCode = 404;
    }
    res.send();
});

MongoClient.connect(mongoUrl, function(err, client) {
    if (err) {
        throw err;
    }
    
    console.log("Connected successfully");

    mongoDb = client.db();


    app.listen(3000, () => {
        console.log('Listening on port 3000');
    });
});